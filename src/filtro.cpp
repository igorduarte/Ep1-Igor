#include "filtro.hpp"

using namespace std;

Filtro::Filtro(){}

Filtro::Filtro(int div, int size, int *filter, char *diretorio){
	this->div=div;
	this->size=size;
	this->filter=filter;
	this->diretorio=diretorio;
}

void Filtro::setDiv(int div){
	this->div=div;
}

int Filtro::getDiv(){
	return div;
}

void Filtro::setSize(int size){
	this->size=size;
}

int Filtro::getSize(){
	return size;
}

void Filtro::setFilter(int *filter){
	this->filter=filter;
}

int *Filtro::getFilter(){
	return filter;
}

void Filtro::setDiretorio(char *diretorio){
	this->diretorio=diretorio;
}

char *Filtro::getDiretorio(){
	return diretorio;
}
