#include "imagem.hpp"

#include <iostream>
#include <fstream>
#include <string>

using namespace std;

Imagem::Imagem(){}

Imagem::Imagem(string numeroMagico, string comentario, int largura, int altura, int nivelMaximoCinza, char *pixels){
	this->numeroMagico=numeroMagico;
	this->comentario=comentario;
	this->largura=largura;
	this->altura=altura;
	this->nivelMaximoCinza=nivelMaximoCinza;
	this->pixels=pixels;
}

void Imagem::setNumeroMagico(string numeroMagico){
	this->numeroMagico=numeroMagico;
}


string Imagem::getNumeroMagico(){
	return numeroMagico;
}

void Imagem::setComentario(string comentario){
	this->comentario=comentario;
}

string Imagem::getComentario(){
	return comentario;
}

void Imagem::setAltura(int altura){
	this->altura=altura;
}

int Imagem::getAltura(){
	return altura;
}

void Imagem::setLargura(int largura){
	this->largura=largura;
}

int Imagem::getLargura(){
	return largura;
}

void Imagem::setNivelMaximoCinza(int nivelMaximoCinza){
	this->nivelMaximoCinza=nivelMaximoCinza;
}

int Imagem::getNivelMaximoCinza(){
	return nivelMaximoCinza;
}

void Imagem::setPixels(char *pixels){
	this->pixels=pixels;
}

char *Imagem::getPixels(){
	return pixels;
}

void Imagem::copiarImagem(string numeroMagico, string comentario, int largura, int altura, int nivelMaximoCinza, char *pixels, char *diretorio){
	ofstream newImgFile(diretorio);

	newImgFile << numeroMagico << endl;
	newImgFile << comentario << endl;
	newImgFile << largura << " " << altura << endl;
	newImgFile << nivelMaximoCinza << endl;

	char *pixelsCopia;
	pixelsCopia=new char[largura*altura];
	pixelsCopia=getPixels();

	for(int i=0; i<largura*altura; i++){
		newImgFile.put(pixelsCopia[i]);
	}
}
