#include <iostream>
#include <sstream>
#include <fstream>
#include <string>

#include "imagem.hpp"
#include "filtro.hpp"
#include "negativo.hpp"
#include "smooth.hpp"
#include "sharpen.hpp"

using namespace std;

int main(){
	string numeroMagico, comentario;
	int largura, altura, nivelMaximoCinza, opcao, filterSmooth[]={1, 1, 1, 1, 1, 1, 1, 1, 1},filterSharpen[9]={0, -1, 0, -1, 5, -1, 0, -1, 0};
	char *pixels, diretorio[1000];
	stringstream temp;

	cout << "Digite o caminho da imagem que deseja editar" << endl;
	cin >> diretorio;

	ifstream imgFile(diretorio);

	if(!imgFile){
		cout << "Imagem não encontrada" << endl;
	}else{

		if(imgFile==NULL){
			cout << "A alocação falhou" << endl;
		}else{
		
			getline(imgFile, numeroMagico);
			getline(imgFile, comentario);
			temp << comentario;
			if(comentario[0]=='#'){
				imgFile >> largura >> altura;
			}else{
				temp >> largura >> altura;	
			}
			imgFile >> nivelMaximoCinza;
			pixels=new char[largura*altura];

			for(int i=0; i<largura*altura; i++){
				imgFile.get(pixels[i]);
			}

			comentario="# Ep1 - OO";

			Imagem imagem;
			Negativo negativo;
			Smooth smooth;
			Sharpen sharpen;

			imagem.setNumeroMagico(numeroMagico);
			imagem.setComentario(comentario);
			imagem.setLargura(largura);
			imagem.setAltura(altura);
			imagem.setNivelMaximoCinza(nivelMaximoCinza);
			imagem.setPixels(pixels);

			cout << "Menu:" << endl;
			cout << "1-Copiar imagem" << endl;
			cout << "2-Aplicar negativo na imagem" << endl;
			cout << "3-Aplicar smooth na imagem" << endl;
			cout << "4-Aplicar sharpen na imagem" << endl;
			cin >> opcao;
	
			switch(opcao){
				case 1:
					cout << "Digite o diretório em que deseja salvar a imagem modificada" << endl;
		            cin >> diretorio;
					imagem.copiarImagem(imagem.getNumeroMagico(), imagem.getComentario(), imagem.getLargura(), imagem.getAltura(), imagem.getNivelMaximoCinza(), imagem.getPixels(), diretorio);
					break;
				case 2:
					cout << "Digite o diretório em que deseja salvar a imagem modificada" << endl;
		            cin >> diretorio;
					negativo.setDiretorio(diretorio);
					negativo.aplicarNegativo(imagem.getNumeroMagico(), imagem.getComentario(), imagem.getLargura(), imagem.getAltura(), imagem.getNivelMaximoCinza(), imagem.getPixels(), negativo.getDiretorio());
					break;
				case 3:
					cout << "Digite o diretório em que deseja salvar a imagem modificada" << endl;
		            cin >> diretorio;
					smooth.setFilter(filterSmooth);
					smooth.setSize(3);
					smooth.setDiv(9);
					smooth.setDiretorio(diretorio);
					smooth.aplicarSmooth(imagem.getNumeroMagico(), imagem.getComentario(), imagem.getLargura(), imagem.getAltura(), imagem.getNivelMaximoCinza(), imagem.getPixels(), smooth.getFilter(), smooth.getSize(), smooth.getDiv(), smooth.getDiretorio());
					break;
				case 4:
					cout << "Digite o diretório em que deseja salvar a imagem modificada" << endl;
		            cin >> diretorio;
					sharpen.setFilter(filterSharpen);
					sharpen.setSize(3);
					sharpen.setDiv(1);
					sharpen.setDiretorio(diretorio);
					sharpen.aplicarSharpen(imagem.getNumeroMagico(), imagem.getComentario(), imagem.getLargura(), imagem.getAltura(), imagem.getNivelMaximoCinza(), imagem.getPixels(), sharpen.getFilter(), sharpen.getSize(), sharpen.getDiv(), sharpen.getDiretorio());					
					break;
				default:
					cout << "Opção inválida" << endl;
			}

		}
		
	}

	return 0;
}
